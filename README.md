# LKCAMP Nextcloud

This repository contains the configuration used in our
[Nextcloud](https://nextcloud.com) instance.

## Setting up the instance

1. Install [Docker Engine](https://docs.docker.com/engine) and [Docker
   Compose](https://docs.docker.com/compose).
2. Define the database configuration in a `.env` file in safe location
   defining the password:

    ```
    NEXTCLOUD_DB_PASS=<some-hard-password>
    ```

    Make sure you have this configuration file with the correct access
    permissions. We will assume it is in the repository root to the following
    steps.

3. Start the application services:

    ```bash
    sudo docker compose --env-file .env up -d
    ```

4. Connect to the server with your browser (for instance,
   [https://localhost](https://localhost) if you are running locally), and
   fill-in the options in the web interface:

    * your desired admin `username` and `password`
    * database type: PostgreSQL
    * database user: `nextcloud`
    * database password: the one you defined in the `NEXTCLOUD_DB_PASS`
    * database name: `ncdb`
    * database host: `db_postgres`
